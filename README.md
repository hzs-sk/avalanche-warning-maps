# avalanche-warning-maps

Geodata for rendering avalanche warning maps via [albina-server](https://gitlab.com/albina-euregio/albina-server) and [Mapyrus](https://github.com/simoc/mapyrus).
